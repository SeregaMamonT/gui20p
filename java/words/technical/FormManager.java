package words.technical;

import words.technical.gui.GenericForm;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */
public class FormManager
{
  private static FormManager formManager;
  private Container container;
  private LinkedList<JPanel> formStack = new LinkedList<JPanel>();

  private FormManager(Container container)
  {
    this.container = container;
    initBackground();
  }

  private void initBackground()
  {
    JPanel mainPanel = new JPanel();
    mainPanel.setBackground(Color.LIGHT_GRAY);
    container.add("Center", mainPanel);
    formStack.push(mainPanel);
  }

  static void createInstance(Container mainPanel)
  {
    formManager = new FormManager(mainPanel);
  }

  public static FormManager getInstance()
  {
    return formManager;
  }

  public void showForm(GenericForm form)
  {
    JPanel prevForm = formStack.peek();
    if (prevForm != null) {
      container.remove(prevForm);
    }
    formStack.push(form);
    container.add("Center", form);
    container.validate();
  }

  public Component getTopForm()
  {
    return formStack.peek();
  }
}
