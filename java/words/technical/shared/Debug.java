package words.technical.shared;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * User: salexetovich
 * Date: 02.05.14
 */
public class Debug
{
  private final Logger logger;
  private static Debug instance;


  private Debug()
  {
    this.logger = Logger.getLogger("words");
    FileHandler fileHandler = null;
    try {
      fileHandler = new FileHandler("logs/log", true);
      fileHandler.setFormatter(new SimpleFormatter());
    }
    catch (IOException e) {
      // ignore
    }
    logger.addHandler(fileHandler);
  }


  public static Debug getInstance()
  {
    if (instance == null) {
      instance = new Debug();
    }
    return instance;
  }


  public void log(String message)
  {
    logger.warning(message);
  }


  public void logThrowable(Throwable th)
  {
    log(formatStackTrace(th));
  }


  private static String formatStackTrace(Throwable th)
  {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    PrintStream stream = new PrintStream(out);
    th.printStackTrace(stream);
    stream.close();
    return new String(out.toByteArray());
  }
}
