package words.technical.shared;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

/**
 * User: salexetovich
 * Date: 24.04.14
 */
public class Translator
{
  private static Properties translationTable;


  public static void loadBundle(String bundleName)
  {
    if (translationTable != null) {
      throw new IllegalStateException("Translation table is already loaded!");
    }
    translationTable = new Properties();
    try {
      InputStreamReader reader = new InputStreamReader(new FileInputStream(bundleName), Charset.forName("UTF-8"));
      translationTable.load(reader);
    }
    catch (IOException e) {
      throw new RuntimeException("Could not read translation table!", e);
    }
  }


  public static String getString(String locution)
  {
    return translationTable.getProperty(locution, "@" + locution);
  }
}
