package words.technical;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import words.technical.gui.component.IMenuItem;
import words.technical.gui.component.Menu;
import words.technical.gui.component.MenuItem;
import words.technical.gui.MenuListener;
import words.technical.shared.Translator;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * User: salexetovich
 * Date: 17.09.13
 */
public class Main
{
  public static void main(String[] args)
  {
    Main main = new Main();
    Translator.loadBundle("resources/locution.properties");
    main.createAndShowGui();
  }

  public void createAndShowGui()
  {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    JMenuBar menuBar = createMenuBar();
    frame.getContentPane().add("North", menuBar);
    FormManager.createInstance(frame.getContentPane());
    ((Menu)menuBar.getMenu(0)).emulateClick();

    frame.setSize(800, 600);
    frame.setVisible(true);
  }


  private JMenuBar createMenuBar()
  {
    JMenuBar menuBar = new JMenuBar();
    try {
      DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = documentFactory.newDocumentBuilder();
      Document doc = builder.parse("resources/menustructure.xml");
      Element root = doc.getDocumentElement();

      NodeList nodeList = root.getElementsByTagName("*");
      for (int i = 0; i < nodeList.getLength(); i++) {
        menuBar.add(createMenuItem((Element) nodeList.item(i)));
      }
    }
    catch (Exception ex) {
      throw new RuntimeException(ex);
    }
    return menuBar;
  }

  private JMenuItem createMenuItem(Element node)
  {
    JMenuItem menuItem;
    if (node.getNodeName().equals("menu")) {
      menuItem = new Menu(node.getAttribute("title"));
    }
    else
      if (node.getNodeName().equals("menu-item")) {
        menuItem = new MenuItem(node.getAttribute("title"));
      }
      else {
        throw new IllegalStateException("Illegal node name found: " + node.getNodeName());
      }
    menuItem.addMouseListener(new MenuListener());

    IMenuItem iMenuItem = (IMenuItem) menuItem;
    iMenuItem.setMenuId(node.getAttribute("id"));
    try {
      Class handlerClass = Class.forName(node.getAttribute("handler"));
      iMenuItem.setFormHandlerClass(handlerClass);
    }
    catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
    NodeList nodeList = node.getElementsByTagName("*");
    for (int i = 0; i < nodeList.getLength(); i++) {
      menuItem.add(createMenuItem((Element) nodeList.item(i)));
    }
    return menuItem;
  }

}
