package words.technical.gui;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 9:52
 * To change this template use File | Settings | File Templates.
 */
public class GenericFormController
{
  GenericForm form;

  public void setForm(GenericForm form)
  {
    this.form = form;
  }

  public GenericForm getForm()
  {
    return form;
  }
}
