package words.technical.gui;

import words.technical.gui.component.IMenuItem;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 9:38
 * To change this template use File | Settings | File Templates.
 */
public class MenuListener implements MouseListener
{
  @Override
  public void mouseClicked(MouseEvent e)
  {
  }

  @Override
  public void mousePressed(MouseEvent e)
  {
    IMenuItem menuItem = (IMenuItem) e.getSource();
    GenericFormHandler formHandler = createFormHandler(menuItem.getFormHandlerClass());
    formHandler.show();
  }

  @Override
  public void mouseReleased(MouseEvent e)
  {
  }

  @Override
  public void mouseEntered(MouseEvent e)
  {
  }

  @Override
  public void mouseExited(MouseEvent e)
  {
  }

  private GenericFormHandler createFormHandler(Class<? extends GenericFormHandler> handlerClass)
  {
    try {
      return handlerClass.newInstance();
    }
    catch (InstantiationException e) {
      e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    }
    catch (IllegalAccessException e) {
      e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    }
    return null;
  }
}
