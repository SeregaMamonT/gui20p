package words.technical.gui;

import words.technical.FormManager;
import words.technical.shared.Debug;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Method;
import java.util.EventObject;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 24.09.13
 * Time: 12:38
 * To change this template use File | Settings | File Templates.
 */
public class GuiUtilities
{
  public static void fireEvent(EventObject event, String methodName)
  {
    if (methodName != null) {
      GenericFormController controller = getForm((Component) event.getSource()).getController();
      try {
        Method method = controller.getClass().getMethod(methodName, EventObject.class);
        method.invoke(controller, event);
      }
      catch (Throwable th) {
        Debug.getInstance().logThrowable(th);
        JOptionPane.showMessageDialog(FormManager.getInstance().getTopForm(), th.getCause(), "Error!", JOptionPane.ERROR_MESSAGE);
      }
    }
  }


  private static GenericForm getForm(Component component)
  {
    Component form = component;
    while (!(form instanceof GenericForm)) {
      form = form.getParent();
    }
    return (GenericForm) form;
  }
}
