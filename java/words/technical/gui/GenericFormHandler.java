package words.technical.gui;

import words.technical.FormManager;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 9:36
 * To change this template use File | Settings | File Templates.
 */
public abstract class GenericFormHandler
{
  protected GenericFormController formController;
  protected GenericForm form;

  public abstract GenericFormController getFormController();

  public abstract GenericForm getForm();

  public void show()
  {
    try {
      formController = getFormController();
      form = getForm();
      formController.setForm(form);
      form.setControler(formController);
      FormManager.getInstance().showForm(form);
    }
    catch (Exception e) {
      e.printStackTrace();
      JOptionPane.showMessageDialog(FormManager.getInstance().getTopForm(), e.getCause(), "Error!", JOptionPane.ERROR_MESSAGE);
    }
  }
}
