package words.technical.gui;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 10:06
 * To change this template use File | Settings | File Templates.
 */
public class GenericForm extends JPanel
{
  GenericFormController controler;

  public GenericFormController getController()
  {
    return controler;
  }

  public void setControler(GenericFormController controler)
  {
    this.controler = controler;
  }
}
