package words.technical.gui.component.chart;

import org.jfree.data.xy.XYSeries;

/**
 * User: MamonT
 * Date: 03.06.14
 */
public class LabeledXYSeries extends XYSeries
{
  public LabeledXYSeries(Comparable key)
  {
    super(key);
  }

  public void add(double x, double y, String label)
  {
    super.add(new LabeledXYDataItem(x, y, label));
  }


  public String getLabel(int item)
  {
    return ((LabeledXYDataItem)getDataItem(item)).getLabel();
  }
}
