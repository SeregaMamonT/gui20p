package words.technical.gui.component.chart;

import org.jfree.data.xy.XYDataItem;

/**
  * User: MamonT
  * Date: 04.06.14
  */
public class LabeledXYDataItem extends XYDataItem
{
  private String label;

  public LabeledXYDataItem(Number x, Number y, String label)
  {
    super(x, y);
    this.label = label;
  }


  public String getLabel()
  {
    return label;
  }
}
