package words.technical.gui.component.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.TextAnchor;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * User: MamonT
 * Date: 03.06.14
 */
public class ChartUtil
{
  public static JFreeChart createLabeledXYChart(String title, String xAxis, String yAxis, LabeledXYDataset dataset)
  {
    JFreeChart chart = ChartFactory.createScatterPlot(title, xAxis, yAxis, dataset);

    XYPlot plot = (XYPlot) chart.getPlot();

    XYItemRenderer renderer = plot.getRenderer();
    renderer.setBaseItemLabelGenerator(new LabelGenerator());
    renderer.setBaseItemLabelPaint(Color.BLUE.darker());
    renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE4, TextAnchor.CENTER));
    renderer.setBaseItemLabelsVisible(true);

    renderer.setBaseItemLabelFont(new Font("labelFont", Font.PLAIN, 16));

    renderer.setBaseToolTipGenerator(new StandardXYToolTipGenerator());

    plot.setRenderer(renderer);
    return chart;
  }


  public static class LabeledXYDataset extends AbstractXYDataset
  {
    private List<LabeledXYSeries> seriesList = new ArrayList<LabeledXYSeries>();


    public void add(LabeledXYSeries series)
    {
      seriesList.add(series);
    }


    public String getLabel(int series, int item)
    {
      return seriesList.get(series).getLabel(item);
    }


    @Override
    public int getSeriesCount()
    {
      return seriesList.size();
    }


    @Override
    public Comparable getSeriesKey(int series)
    {
      return seriesList.get(series).getKey();
    }


    @Override
    public int getItemCount(int series)
    {
      return seriesList.get(series).getItemCount();
    }


    @Override
    public Number getX(int series, int item)
    {
      return seriesList.get(series).getX(item);
    }


    @Override
    public Number getY(int series, int item)
    {
      return seriesList.get(series).getY(item);
    }
  }


  private static class LabelGenerator implements XYItemLabelGenerator
  {
    @Override
    public String generateLabel(XYDataset dataset, int series, int item)
    {
      return ((LabeledXYDataset) dataset).getLabel(series, item);
    }
  }
}
