package words.technical.gui.component;

import words.technical.gui.GuiUtilities;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 14.10.13
 * Time: 14:37
 * To change this template use File | Settings | File Templates.
 */
public class MTextField extends JTextField
{
  private String listenerMethod;

  public MTextField()
  {
    initialize();
  }

  private void initialize()
  {
    addKeyListener(new KeyAdapter()
    {
      @Override
      public void keyTyped(KeyEvent e)
      {
        GuiUtilities.fireEvent(e, listenerMethod);
      }
    });
  }

  public String getListenerMethod()
  {
    return listenerMethod;
  }

  public void setListenerMethod(String listenerMethod)
  {
    this.listenerMethod = listenerMethod;
  }
}
