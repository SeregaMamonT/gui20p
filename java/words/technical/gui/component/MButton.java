package words.technical.gui.component;

import words.technical.gui.GuiUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 25.09.13
 * Time: 13:05
 * To change this template use File | Settings | File Templates.
 */
public class MButton extends JButton
{
  private String listenerMethod;

  public MButton(String name)
  {
    super(name);
    initialize();
  }

  public MButton()
  {
    this(null);
  }

  private void initialize()
  {
    addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent e)
      {
        GuiUtilities.fireEvent(e, listenerMethod);
      }
    });
    setMargin(new Insets(0, 0, 0, 0));
  }

  public String getListenerMethod()
  {
    return listenerMethod;
  }

  public void setListenerMethod(String listenerMethod)
  {
    this.listenerMethod = listenerMethod;
  }
}
