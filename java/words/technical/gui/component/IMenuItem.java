package words.technical.gui.component;

import words.technical.gui.GenericFormHandler;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 9:12
 * To change this template use File | Settings | File Templates.
 */
public interface IMenuItem
{
  public String getMenuId();

  public void setMenuId(String menuId);

  public Class<? extends GenericFormHandler> getFormHandlerClass();

  public void setFormHandlerClass(Class<? extends GenericFormHandler> handlerClass);
}
