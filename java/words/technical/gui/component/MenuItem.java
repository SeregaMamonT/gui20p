package words.technical.gui.component;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 9:14
 * To change this template use File | Settings | File Templates.
 */
public class MenuItem extends JMenuItem implements IMenuItem
{
  protected String menuId;
  protected Class handlerClass;

  public MenuItem(String name)
  {
    super(name);
  }

  @Override
  public String getMenuId()
  {
    return menuId;
  }

  @Override
  public void setMenuId(String menuId)
  {
    this.menuId = menuId;
  }

  @Override
  public Class getFormHandlerClass()
  {
    return handlerClass;
  }

  @Override
  public void setFormHandlerClass(Class handlerClass)
  {
    this.handlerClass = handlerClass;
  }
}
