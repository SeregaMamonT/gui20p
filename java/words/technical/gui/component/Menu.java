package words.technical.gui.component;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 17.09.13
 * Time: 9:13
 * To change this template use File | Settings | File Templates.
 */
public class Menu extends JMenu implements IMenuItem
{
  protected String menuId;
  protected Class handlerClass;

  public Menu(String name)
  {
    super(name);
  }

  @Override
  public String getMenuId()
  {
    return menuId;
  }

  @Override
  public void setMenuId(String menuId)
  {
    this.menuId = menuId;
  }

  @Override
  public Class getFormHandlerClass()
  {
    return handlerClass;
  }

  @Override
  public void setFormHandlerClass(Class handlerClass)
  {
    this.handlerClass = handlerClass;
  }


  public void emulateClick()
  {
    MouseEvent event = new MouseEvent(this, MouseEvent.MOUSE_PRESSED, new Date().getTime(), InputEvent.BUTTON1_MASK, 20, 15, 24, 38, 1, false, 1);
    super.processMouseEvent(event);
    setSelected(false);
  }
}
