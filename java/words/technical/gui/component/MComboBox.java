package words.technical.gui.component;

import words.technical.gui.GuiUtilities;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 24.09.13
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
public class MComboBox extends JComboBox
{
  private String listenerMethod;

  public MComboBox()
  {
    super();
    initialize();
  }

  private void initialize()
  {
    addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent e)
      {
        GuiUtilities.fireEvent(e, listenerMethod);
      }
    });
  }

  public String getListenerMethod()
  {
    return listenerMethod;
  }

  public void setListenerMethod(String listenerMethod)
  {
    this.listenerMethod = listenerMethod;
  }
}
